﻿using System.Linq;
using System.Collections.Generic;

using Karen90MmoFramework.Hud;
using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Data;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class QuestManager
	{
		#region Constants and Fields

		private readonly Dictionary<short, int> keys;
		private readonly ActiveQuest[] values;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the count
		/// </summary>
		public int Count
		{
			get
			{
				return keys.Count;
			}
		}

		#endregion

		#region Constructors and Destructors

		public QuestManager()
		{
			this.keys = new Dictionary<short, int>();
			this.values = new ActiveQuest[GameSettings.MAX_ACTIVE_QUESTS];
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Adds an active quest
		/// </summary>
		public void AddQuest(short questId, QuestStatus questStatus, byte[] counts)
		{
			var index = this.GetNextAvailableIndex();
			this.keys.Add(questId, index);
			this.values[index] = new ActiveQuest(questId, questStatus, counts);

			HUD.Instance.AutoTrackQuest(questId);
		}

		/// <summary>
		/// Starts a specific quest
		/// </summary>
		/// <param name="questId"></param>
		public void StartQuest(short questId)
		{
			QuestData quest;
			if (MmoItemDataStorage.Instance.TryGetQuest(questId, out quest))
			{
				var activeQuest = new ActiveQuest(questId, QuestStatus.InProgress, null);

				var objCounts = quest.ObjectiveCounts;
				if (objCounts != null)
					activeQuest.Counts = new byte[objCounts.Length];

				var index = this.GetNextAvailableIndex();
				this.keys.Add(quest.QuestId, index);
				this.values[index] = activeQuest;

				HUD.Instance.QueueNotification(NotificationType.Quest_Started, quest.Name);

				if (CanCompleteQuest(questId))
					this.CompleteQuest(questId);

				WindowManager.Instance.RefreshJournalMenu();
				HUD.Instance.AutoTrackQuest(quest.QuestId);
			}
		}

		/// <summary>
		/// Finishes a specific quest
		/// </summary>
		/// <param name="questId"></param>
		public void FinishQuest(short questId)
		{
			QuestData quest;
			if (MmoItemDataStorage.Instance.TryGetQuest(questId, out quest))
			{
				var index = this.keys[quest.QuestId];
				if (keys.Remove(quest.QuestId))
					this.values[index] = null;

				WindowManager.Instance.RefreshJournalMenu();
				HUD.Instance.QueueNotification(NotificationType.Quest_Completed, quest.Name);
				HUD.Instance.UntrackQuest(quest.QuestId);
			}
		}

		/// <summary>
		/// Progresses a quest
		/// </summary>
		public void ProgressQuest(short questId, byte index, byte count)
		{
			QuestData quest;
			if (MmoItemDataStorage.Instance.TryGetQuest(questId, out quest))
			{
				int sIndex;
				if (keys.TryGetValue(questId, out sIndex))
				{
					string message;

					var counts = this.values[sIndex].Counts;
					var oCount = quest.ObjectiveCounts[index];

					counts[index] = count;
					if (counts[index] >= oCount)
					{
						message = "Completed: " + quest.Objectives[index];
					}
					else
					{
						message = quest.Objectives[index] + ": " + count + " / " + oCount;
					}

					HUD.Instance.QueueNotification(NotificationType.Quest_Progress, message);

					if (CanCompleteQuest(questId))
						this.CompleteQuest(questId);

					HUD.Instance.UpdateQuest(questId);
				}
			}
		}

		/// <summary>
		/// Gets all the active quests
		/// </summary>
		public ActiveQuest[] GetQuests()
		{
			return this.values;
		}

		/// <summary>
		/// Gets a quest by its id
		/// </summary>
		public ActiveQuest GetQuest(short questId)
		{
			int index;
			if (keys.TryGetValue(questId, out index))
				return values[index];

			return default(ActiveQuest);
		}

		/// <summary>
		/// Gets an active quest by index
		/// </summary>
		public ActiveQuest GetQuestByIndex(int index)
		{
			int counter = 0;
			for (int i = 0; i < this.values.Length; i++)
			{
				var val = values[i];
				if (val.Status != QuestStatus.None)
				{
					if (index == counter)
						return val;

					counter++;
				}
			}

			return default(ActiveQuest);
		}

		/// <summary>
		/// Determines whether a quest can be completed or not
		/// </summary>
		bool CanCompleteQuest(short questId)
		{
			QuestData quest;
			if (MmoItemDataStorage.Instance.TryGetQuest(questId, out quest))
			{
				int index;
				if (keys.TryGetValue(questId, out index))
				{
					var sCounts = this.values[index].Counts;
					var qCounts = quest.ObjectiveCounts;

					if (qCounts == null)
						return true;

					return !qCounts.Where((t, i) => sCounts[i] < t).Any();
				}
			}

			return false;
		}

		/// <summary>
		/// Completes a quest
		/// </summary>
		void CompleteQuest(short questId)
		{
			QuestData quest;
			if (MmoItemDataStorage.Instance.TryGetQuest(questId, out quest))
			{
				int index;
				if (keys.TryGetValue(questId, out index))
				{
					this.values[index].Status = QuestStatus.TurnIn;

					WindowManager.Instance.RefreshJournalMenu();
					HUD.Instance.QueueNotification(NotificationType.Quest_Progress, quest.FinalObjective);
				}
			}
		}

		/// <summary>
		/// Gets the next available empty slot index
		/// </summary>
		int GetNextAvailableIndex()
		{
			for (int i = 0; i < GameSettings.MAX_ACTIVE_QUESTS; i++)
			{
				if (values[i] == null)
					return i;
			}

			return -1;
		}

		#endregion
	}
}
