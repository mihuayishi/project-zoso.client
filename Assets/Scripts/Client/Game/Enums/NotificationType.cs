﻿namespace Karen90MmoFramework.Client.Game
{
	public enum NotificationType : byte
	{
		Quest_Started,
		Quest_Completed,
		Quest_Progress,
		Entering,
	};
}