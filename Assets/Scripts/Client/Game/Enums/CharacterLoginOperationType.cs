﻿namespace Karen90MmoFramework.Client.Game
{
	public enum CharacterLoginOperationType
	{
		None = 0,
		CharacterSelection = 1,
		CharacterCreation = 2,
	};
}