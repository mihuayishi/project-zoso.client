﻿using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Data;

namespace Karen90MmoFramework.Client.Game
{
	public class MmoItemDataStorage
	{
		#region Constants and Fields

		private static readonly MmoItemDataStorage _instance = new MmoItemDataStorage();

		private readonly Dictionary<short, MmoItemData> mmoItems;
		private readonly Dictionary<short, SpellData> spells;
		private readonly Dictionary<short, QuestData> quests;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the singleton instance
		/// </summary>
		public static MmoItemDataStorage Instance
		{
			get
			{
				return _instance;
			}
		}

		#endregion

		#region Constructors and Destructors

		private MmoItemDataStorage()
		{
			this.mmoItems = new Dictionary<short, MmoItemData>();
			this.spells = new Dictionary<short, SpellData>();
			this.quests = new Dictionary<short, QuestData>();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Loads the cache
		/// </summary>
		public void Load()
		{
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 10,
					ItemType = MmoItemType.Consumable,
					Name = "Health Potion",
					Level = 1,
					Rarity = Rarity.Uncommon,
					BuyoutPrice = 14,
					SellPrice = 8,
					MaxStack = 20,
					IsTradable = true,
					UseSpellId = 11,
					UseLimit = UseLimit.Once,
					Description = "Might cause death if used too much",
					IconId = 1,
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 11,
					ItemType = MmoItemType.Consumable,
					Name = "Power Potion",
					Level = 1,
					Rarity = Rarity.Uncommon,
					BuyoutPrice = 14,
					SellPrice = 8,
					MaxStack = 20,
					IsTradable = true,
					UseSpellId = 12,
					UseLimit = UseLimit.Once,
					Description = "Might cause death if used too much",
					IconId = 2
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 12,
					ItemType = MmoItemType.Generic,
					Name = "Wolf Pelt",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 12,
					MaxStack = 20,
					IsTradable = true,
					Description = "Merchants will pay high price for this",
					IconId = 4
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 13,
					ItemType = MmoItemType.Generic,
					Name = "Kings' Foil",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 4,
					MaxStack = 40,
					IsTradable = true,
					Description = "An ordinary herb used in making potions",
					IconId = 3
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 14,
					ItemType = MmoItemType.Generic,
					Name = "Basil",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 6,
					MaxStack = 40,
					IsTradable = true,
					Description = "Used in many cooking recipes",
					IconId = 3
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 15,
					ItemType = MmoItemType.Generic,
					Name = "Potato",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 4,
					MaxStack = 40,
					IsTradable = true,
					Description = "Used in many cooking recipes",
					IconId = 3
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 16,
					ItemType = MmoItemType.Generic,
					Name = "Carrot",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 4,
					MaxStack = 40,
					IsTradable = true,
					Description = "Used in many cooking recipes",
					IconId = 3
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 17,
					ItemType = MmoItemType.Generic,
					Name = "Onion",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 4,
					MaxStack = 40,
					IsTradable = true,
					Description = "Used in many cooking recipes",
					IconId = 3
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 18,
					ItemType = MmoItemType.Generic,
					Name = "Wolf Meat",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 9,
					MaxStack = 20,
					IsTradable = true,
					Description = "Tender meat if cooked right will provide valuable nutrients",
					IconId = 3
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 19,
					ItemType = MmoItemType.Generic,
					Name = "Sharp Teeth",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 5,
					MaxStack = 60,
					IsTradable = true,
					Description = "Very dangerous if someone manages to attach this to a weapon",
					IconId = 3
				});
			this.AddMmoItem(new MmoItemData
				{
					ItemId = 20,
					ItemType = MmoItemType.Generic,
					Name = "Sharp Claw",
					Level = 1,
					Rarity = Rarity.Common,
					SellPrice = 4,
					MaxStack = 60,
					IsTradable = true,
					Description = "Very dangerous if someone manages to attach this to a weapon",
					IconId = 3
				});

			this.AddSpell(new SpellData
				{
					SpellId = 0,
					Name = "Using...",
					School = SpellSchools.Normal,
					RequiredTargetType = SpellTargetTypes.None,
					RequiredWeaponType = WeaponTypes.None,
					TargetSelectionMethod = SpellTargetSelectionMethods.SingleTarget,
					AffectedByGCD = false,
					IsProc = false,
					TriggersGCD = false,
					CastTime = 2,
					IconId = 7,
				});
			this.AddSpell(new SpellData
				{
					SpellId = 10,
					Name = "Glancing Strike",
					School = SpellSchools.Normal,
					RequiredTargetType = SpellTargetTypes.HostileUnit,
					RequiredWeaponType = WeaponTypes.TwoHandedMelee | WeaponTypes.OneHandedMelee | WeaponTypes.Staff,
					TargetSelectionMethod = SpellTargetSelectionMethods.SingleTarget,
					AffectedByGCD = true,
					IsProc = false,
					TriggersGCD = true,
					PowerType = Vitals.Power,
					PowerCost = 6,
					Cooldown = 1,
					CastTime = 0,
					MinCastRadius = 0,
					MaxCastRadius = 10,
					Description = "Strike the target with your weapon lightly",
					IconId = 8,
				});
			this.AddSpell(new SpellData
				{
					SpellId = 11,
					Name = "itm_10",
					School = SpellSchools.Normal,
					RequiredTargetType = SpellTargetTypes.Self,
					RequiredWeaponType = WeaponTypes.None,
					TargetSelectionMethod = SpellTargetSelectionMethods.SingleTarget,
					AffectedByGCD = false,
					IsProc = false,
					TriggersGCD = false,
					PowerType = Vitals.Power,
					PowerCost = 0,
					Cooldown = 10,
					CastTime = 0,
					MinCastRadius = 0,
					MaxCastRadius = 10,
					Description = "Increases the health by a small amount"
				});
			this.AddSpell(new SpellData
				{
					SpellId = 12,
					Name = "itm_11",
					School = SpellSchools.Normal,
					RequiredTargetType = SpellTargetTypes.Self,
					RequiredWeaponType = WeaponTypes.None,
					TargetSelectionMethod = SpellTargetSelectionMethods.SingleTarget,
					AffectedByGCD = false,
					IsProc = false,
					TriggersGCD = false,
					PowerType = Vitals.Power,
					PowerCost = 0,
					Cooldown = 10,
					CastTime = 0,
					MinCastRadius = 0,
					MaxCastRadius = 10,
					Description = "Increases the power by a small amount"
				});
			this.AddSpell(new SpellData
				{
					SpellId = 13,
					Name = "Firebolt",
					School = SpellSchools.Fire,
					RequiredTargetType = SpellTargetTypes.HostileUnit,
					RequiredWeaponType = WeaponTypes.None,
					TargetSelectionMethod = SpellTargetSelectionMethods.SingleTarget,
					AffectedByGCD = true,
					IsProc = false,
					TriggersGCD = true,
					PowerType = Vitals.Power,
					PowerCost = 20,
					Cooldown = 5,
					CastTime = 1,
					MinCastRadius = 5,
					MaxCastRadius = 30,
					Description = "Conjures a fireball which seeks and incinerates your target",
					IconId = 9,
				});
			this.AddSpell(new SpellData
				{
					SpellId = 14,
					Name = "Heal",
					School = SpellSchools.Normal,
					RequiredTargetType = SpellTargetTypes.Self | SpellTargetTypes.FriendlyUnit,
					RequiredWeaponType = WeaponTypes.None,
					TargetSelectionMethod = SpellTargetSelectionMethods.SingleTarget,
					AffectedByGCD = true,
					IsProc = false,
					TriggersGCD = true,
					PowerType = Vitals.Power,
					PowerCost = 20,
					Cooldown = 20,
					CastTime = 1,
					MinCastRadius = 0,
					MaxCastRadius = 20,
					Description = "Heals any minor and moderate wounds",
					IconId = 5,
				});

			this.AddQuest(new QuestData
				{
					QuestId = 10,
					Name = "A Friend in Trouble",
					Level = 1,
					RewardXp = 40,
					RewardMoney = 20,
					QuestIntroMsg =
						"Hello stranger!\n\nMe and Kyle has been having some problems lately. Kyle has been acting so strange lately. I want you to go talk to him and see what is troubling him. It is possible that it has to do with the\n\n...\n\nEh, the evil that has been spreading throughout these forsaken lands. I want you to be careful and not to assult him if he becomes, eh, un-stable. Not to worry he will not harm you.\n\nOne more thing do not let him know that I sent you to investigate the matter, because be may not tell you the truth.\n\nI hope you will come back with good news.",
					QuestProgressMsg = "Haven't talked to Kyle yet? Please be hasty the saftey of the town maybe in your hands.",
					QuestCompleteMsg =
						"There you are, I was worried that you might not show up...\n\nWhat news from Kyle?\n\n...\n\nI see, the matter is worse than I thought. I am afraid this is not over yet and the problem at hand is getting worse. We need to act quickly or else, Uh.. I try not to think about it. I am afraid I am going to need you to ask you another favour for the sake of the saftey of the town but for now take some rest.",
					FinalObjective = "Return to Stan",
					Objectives = new[] {"Talk to Kyle"},
					ObjectiveCounts = new byte[] {1},
				});
			this.AddQuest(new QuestData
				{
					QuestId = 11,
					Name = "Finding the Source",
					Level = 1,
					RewardXp = 60,
					RewardMoney = 30,
					RewardItems = new[] { new ItemStructure(10, 1), new ItemStructure(11, 1) },
					QuestIntroMsg =
						"It is good to see you again, I am afraid Kyle is getting worse; unless we do something he will turn into something un-natural.\n\nOk, you said he is feeling unstable after he came back from the forest correct? Then we need to find the source of it which, I am afraid may lie deep into the forest. Whomever or whatever doing this has an intention of spreading evil throughout these lands. If we do not stop it, it may affect the rest of the town. Please be hasty and find out what is causing it. In the meantime I will go see Kyle, becareful.",
					QuestProgressMsg =
						"Did you find out what is causing all these? Kyle is getting worse if you do not find the root of it I am afraid we will loose him forever.",
					QuestCompleteMsg =
						"Ahh...You did it\n\nyou... did... it...\n\nWe all felt it. After you destroyed whatever it was Kyle came back to normal.\n\nI am also curious to know what it is that caused all these. A void monster you say...hmm...This could only mean one thing. There was a talk of the return of the 'Void Lord' and yes it was a legand. However after the monster attack I am beginning to worry that it may be possible. Give me some time while I go look through the old archives.\n\nAnd here take these for your troubles, it isn't much but I am sure you will find better use for it than myself.",
					FinalObjective = "Return to Stan",
					Objectives = new[] {"Kill the Hungry Monster"},
					ObjectiveCounts = new byte[] {1},
				});
			this.AddQuest(new QuestData
				{
					QuestId = 12,
					Name = "Coming of the Void Lord",
					Level = 1,
					RewardXp = 30,
					RewardMoney = 10,
					QuestIntroMsg =
						"Ok so I went through all my notes and even the oldest archives I can find. However, none of them mentions anything about the Void Lord. There is a wise man named Kenny lives next door. There has been a lot of talk of that mysterious person in town of having strange powers. I heared him mentioning about the Void Lord and the end of the world.\n\nI..I..am afraid that is all I can offer at the moment without any further information. Talk to him and see what he has to offer.\n\nI have hope in you...",
					QuestProgressMsg = "Did you talk to Kenny yet?",
					QuestCompleteMsg =
						"Hello there outsider. I see that you have come to seek answers whether the coming of the Void Lord is true or not.\n\n(He laughs)\n\nHow do I know does not matter now we need to act quickly if we were to stop his arrival.I can feel that u have magical powers in you but you look like a normal person to me. Give me a moment.\n\n(He whispers some strange words)\n\nPerhaps it is true, If you want to help the town in protecting it from destruction talk to me.",
					FinalObjective = "Report to Kenny",
					Objectives = new[] {string.Empty},
				});
			this.AddQuest(new QuestData
				{
					QuestId = 13,
					Name = "Gathering Supplies",
					Level = 2,
					RewardXp = 50,
					RewardMoney = 20,
					RewardItems = new[] { new ItemStructure(10, 2) },
					QuestIntroMsg =
						"Hey... You look like someone that could help me.\n\nThis place is desolate and we rarely get any supplies if we are lucky. It has been over two weeks since we got any supplies. We can gather food easily but cannot go on without thick cloths to withstand the wicked cold in the night time. These nasty wolfs have become so aggressive and we are in short supply of wolf pelts. So I think it is wise to exterminate them.\n\nIf you could dispose some of it and bring their pelt back to me, it would be a great help not only to me but everyone who lives here.\n\nBecareful, these are no ordinary wolfs. They have scary red eyes, sharp claws and even their skin is thicker than a boar's tusk. They are very agile and ferocious, come to think of it I do not have the faintest idea of how they became such dangerous beasts. Well, I am sure you can handle them.\n\nI hear Garrison is selling weapons that could be useful in handling these beasts.",
					QuestProgressMsg =
						"Did you get enough supplies yet?\n\nWe cannot tolerate these wolfs anymore, please do something.",
					QuestCompleteMsg =
						"Ahh... alas\n\nYou have returned, and I see that you have brought some pelts for us.\n\nAnd I am sure that you killed quite a few of them?\n\nExcellent, this will make the wild less dangerous, but not completely safe I'm afraid. We need more people like you to help build a settlement here.",
					FinalObjective = "Deliver Wolf Pelts to Kenny",
					Objectives = new[] {"Collect Wolf Pelts", "Kill Infesting Wolves"},
					ObjectiveCounts = new byte[] {4, 6},
				});
			this.AddQuest(new QuestData
				{
					QuestId = 14,
					Name = "Desperate Times",
					Level = 1,
					RewardXp = 20,
					RewardMoney = 15,
					QuestIntroMsg =
						"Ok so I went through all my notes and even the oldest archives I can find. However, none of them mentions anything about the Void Lord. There is a wise man named Kenny lives next door. There has been a lot of talk of that mysterious person in town of having strange powers. I heared him mentioning about the Void Lord and the end of the world.\n\nI..I..am afraid that is all I can offer at the moment without any further information. Talk to him and see what he has to offer.\n\nI have hope in you...",
					QuestProgressMsg = "Did you gather enough herbs yet?",
					QuestCompleteMsg =
						"Hello there outsider. I see that you have come to seek answers whether the coming of the Void Lord is true or not.\n\n(He laughs)\n\nHow do I know does not matter now we need to act quickly if we were to stop his arrival.I can feel that u have magical powers in you but you look like a normal person to me. Give me a moment.\n\n(He whispers some strange words)\n\nPerhaps it is true, If you want to help the town in protecting it from destruction talk to me.",
					FinalObjective = "Deliver the Herbs to Kyle",
					Objectives = new[] {"Gather Kings' Foils"},
					ObjectiveCounts = new byte[] {4},
				});
		}

		/// <summary>
		/// Unloads the cache
		/// </summary>
		public void Unload()
		{
			this.mmoItems.Clear();
			this.spells.Clear();
			this.quests.Clear();
		}

		/// <summary>
		/// Tries to retrieve an <see cref="MmoItemData"/>.
		/// </summary>
		public bool TryGetMmoItem(short itemId, out MmoItemData mmoItem)
		{
			return this.mmoItems.TryGetValue(itemId, out mmoItem);
		}

		/// <summary>
		/// Tries to retrieve a <see cref="SpellData"/>.
		/// </summary>
		public bool TryGetSpell(short spellId, out SpellData spell)
		{
			return this.spells.TryGetValue(spellId, out spell);
		}

		/// <summary>
		/// Tries to retrieve a <see cref="QuestData"/>.
		/// </summary>
		public bool TryGetQuest(short questId, out QuestData quest)
		{
			return this.quests.TryGetValue(questId, out quest);
		}

		#endregion

		#region Local Methods

		private void AddMmoItem(MmoItemData item)
		{
			this.mmoItems.Add(item.ItemId, item);
		}

		private void AddSpell(SpellData spell)
		{
			this.spells.Add(spell.SpellId, spell);
		}

		private void AddQuest(QuestData quest)
		{
			this.quests.Add(quest.QuestId, quest);
		}

		#endregion
	}
}
