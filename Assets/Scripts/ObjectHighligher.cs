﻿using UnityEngine;

public class ObjectHighligher
{
	private static Color[] colors;
	private static bool changed = false;

	/// <summary>
	/// Highlights the GameObject when called
	/// </summary>
	public static void OnHover(Renderer renderer)
	{
		if (!changed)
		{
			// array to hold previous color info
			colors = new Color[renderer.materials.Length];

			for (int i = 0; i < colors.Length; i++)
			{
				// stores the previous color info
				colors[i] = renderer.materials[i].GetColor("_Color");
				// sets the new color
				renderer.materials[i].SetColor("_Color", new Color(colors[i].r + .3f, colors[i].g + .3f, colors[i].b + .3f));
			}

			changed = true;
		}
	}

	/// <summary>
	/// Resets the hover on the GameObject
	/// </summary>
	public static void OnDehover(Renderer renderer)
	{
		if (changed)
		{
			for (int i = 0; i < colors.Length; i++)
			{
				// resets the color information
				renderer.materials[i].SetColor("_Color", colors[i]);
			}

			colors = null;
			changed = false;
		}
	}

};