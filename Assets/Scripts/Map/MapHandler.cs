﻿using UnityEngine;

using System.Collections;

namespace Karen90MmoFramework.Client
{
	public class MapHandler
	{
		#region Constants and Fields

		private readonly MinimapSystem mapSystem;
		private readonly AssetBundle bundle;
		private readonly MapSettings mapSettings;

		private readonly MapSegment[,] loadedSegments = new MapSegment[2, 2];

		private readonly int mapLayer;

		Rect mapBounds;
		Vector3 mapOffset;

		#endregion

		#region Properties

		MapSegment bottomLeft
		{
			get
			{
				return this.loadedSegments[0, 0];
			}
		}

		MapSegment bottomRight
		{
			get
			{
				return this.loadedSegments[1, 0];
			}
		}

		MapSegment topLeft
		{
			get
			{
				return this.loadedSegments[0, 1];
			}
		}

		MapSegment topRight
		{
			get
			{
				return this.loadedSegments[1, 1];
			}
		}

		#endregion

		#region Constructors and Destructors

		public MapHandler(MinimapSystem mapSystem, AssetBundle mapAsset, MapSettings mapSettings, int mapLayer)
		{
			this.mapSystem = mapSystem;
			this.bundle = mapAsset;
			this.mapSettings = mapSettings;

			this.mapOffset = new Vector3(mapSettings.length / 2f, 0, mapSettings.width / 2f);
			this.mapBounds = new Rect();

			this.loadedSegments = new MapSegment[2, 2];

			this.mapLayer = mapLayer;
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Starts the map
		/// </summary>
		/// <param name="position"></param>
		public void Start(Vector3 position)
		{
			this.PrepareMapAt(position);
		}

		/// <summary>
		/// Updates the actual map. This method is slow it should not be called regularly.
		/// </summary>
		/// <param name="position"></param>
		public void UpdateMap(Vector3 position)
		{
			this.UpdateMapAt(position);
		}

		/// <summary>
		/// Unloads all map and frees resources
		/// </summary>
		public void Unload()
		{
			this.bundle.Unload(true);
		}

		#endregion

		#region Local Methods

		void PrepareMapAt(Vector3 pos)
		{
			this.mapBounds = GetMapBoundsForPos(pos);
			
			this.loadedSegments[0, 0] = new MapSegment() { gameObject = LoadAndCreateSegmentAt(mapBounds.xMin, mapBounds.yMin), state = SegmentState.Active }; // btmLeft
			this.loadedSegments[1, 0] = new MapSegment() { gameObject = LoadAndCreateSegmentAt(mapBounds.xMax, mapBounds.yMin), state = SegmentState.Active }; // btmRight
			this.loadedSegments[0, 1] = new MapSegment() { gameObject = LoadAndCreateSegmentAt(mapBounds.xMin, mapBounds.yMax), state = SegmentState.Active }; // topLeft
			this.loadedSegments[1, 1] = new MapSegment() { gameObject = LoadAndCreateSegmentAt(mapBounds.xMax, mapBounds.yMax), state = SegmentState.Active }; // topRight
		}

		void UpdateMapAt(Vector3 pos)
		{
			var newBounds = this.GetMapBoundsForPos(pos);
			bool changed = false;

			if (newBounds.xMin < mapBounds.xMin)
			{
				this.topRight.Replace(topLeft);
				this.bottomRight.Replace(bottomLeft);
				this.topLeft.Reset();
				this.bottomLeft.Reset();

				changed = true;
			}
			else if (newBounds.xMax > mapBounds.xMax)
			{
				this.topLeft.Replace(topRight);
				this.bottomLeft.Replace(bottomRight);
				this.topRight.Reset();
				this.bottomRight.Reset();

				changed = true;
			}

			if (newBounds.yMin < mapBounds.yMin)
			{
				this.topLeft.Replace(bottomLeft);
				this.topRight.Replace(bottomRight);
				this.bottomLeft.Reset();
				this.bottomRight.Reset();

				changed = true;
			}
			else if (newBounds.yMax > mapBounds.yMax)
			{
				this.bottomLeft.Replace(topLeft);
				this.bottomRight.Replace(topRight);
				this.topLeft.Reset();
				this.topRight.Reset();

				changed = true;
			}

			if (changed)
			{
				this.mapBounds = newBounds;

				this.HandleSegmentAt(bottomLeft, mapBounds.xMin, mapBounds.yMin);
				this.HandleSegmentAt(bottomRight, mapBounds.xMax, mapBounds.yMin);
				this.HandleSegmentAt(topLeft, mapBounds.xMin, mapBounds.yMax);
				this.HandleSegmentAt(topRight, mapBounds.xMax, mapBounds.yMax);
			}
		}

		void HandleSegmentAt(MapSegment segment, float x, float z)
		{
			if (segment.state == SegmentState.Destroyed)
			{
				this.mapSystem.StartAsyncMethod(this.WaitUntilSegmentLoadAt(segment, x, z));
			}
		}

		IEnumerator WaitUntilSegmentLoadAt(MapSegment segment, float x, float z)
		{
			segment.state = SegmentState.Loading;

			var segCoord = GetSegmentCoordForPos(x, z);
			var asyncRequest = this.LoadSegmentAsyncAt((int)segCoord.x, (int)segCoord.y);

			yield return asyncRequest;

			segment.gameObject = CreateSegmentAt(segCoord, asyncRequest.asset as GameObject);
			segment.state = SegmentState.Active;
		}

		Rect GetMapBoundsForPos(Vector3 pos)
		{
			return GetMapBoundsForPos(pos.x, pos.z);
		}

		Rect GetMapBoundsForPos(float x, float z)
		{
			var currSegCoord = this.GetSegmentCoordForPos(x, z);
			
			Rect bounds = new Rect(currSegCoord.x, currSegCoord.y, mapSettings.length, mapSettings.width);
			if (bounds.xMax > mapSettings.xMax)
			{
				var val = bounds.xMax - mapSettings.xMax;
				bounds.x = this.GetSegmentCoordForPos(bounds.xMin - val, z).x;
			}
			else if (x < currSegCoord.x)
			{
				bounds.x -= mapSettings.length;
			}

			if (bounds.yMax > mapSettings.zMax)
			{
				var val = bounds.yMax - mapSettings.zMax;
				bounds.y = this.GetSegmentCoordForPos(x, bounds.yMin - val).y;
			}
			else if (z < currSegCoord.y)
			{
				bounds.y -= mapSettings.width;
			}

			return bounds;
		}

		Vector2 GetSegmentCoordForPos(Vector3 pos)
		{
			return GetSegmentCoordForPos(pos.x, pos.z);
		}

		Vector2 GetSegmentCoordForPos(float x, float z)
		{
			var nX = Mathf.Clamp(x + mapOffset.x, mapSettings.xMin, mapSettings.xMax);
			var nZ = Mathf.Clamp(z + mapOffset.z, mapSettings.zMin, mapSettings.zMax);

			var pX = (int)(nX / mapSettings.length);
			var pZ = (int)(nZ / mapSettings.width);

			var aX = pX * mapSettings.length;
			var aZ = pZ * mapSettings.width;

			return new Vector2(aX, aZ);
		}

		GameObject CreateSegmentAt(Vector2 coord, GameObject segment)
		{
			var go = GameObject.Instantiate(segment) as GameObject;

			go.transform.position = new Vector3(coord.x - mapOffset.x, mapOffset.y, coord.y - mapOffset.z);
			go.layer = this.mapLayer;

			return go;
		}

		GameObject LoadAndCreateSegmentAt(float x, float z)
		{
			var segCoord = GetSegmentCoordForPos(x, z);
			var segment = this.LoadSegmentAt((int)segCoord.x, (int)segCoord.y);

			var go = MonoBehaviour.Instantiate(segment) as GameObject;

			go.transform.position = new Vector3(x - mapOffset.x, mapOffset.y, z - mapOffset.z);
			go.layer = this.mapLayer;

			return go;
		}

		GameObject LoadSegmentAt(int x, int z)
		{
            return bundle.LoadAsset<GameObject>(string.Format("{0}-{1}.{2}", mapSettings.SegmentName, x, z));
		}

		AssetBundleRequest LoadSegmentAsyncAt(int x, int z)
		{
            return bundle.LoadAssetAsync<GameObject>(string.Format("{0}-{1}.{2}", mapSettings.SegmentName, x, z));
		}

		public override string ToString()
		{
			return string.Format("xMin ({0}), xMax ({1}), zMin ({2}), zMax ({3})", mapBounds.xMin, mapBounds.xMax, mapBounds.yMin, mapBounds.yMax);
		}

		#endregion
	};
}
