﻿using UnityEngine;

public static class TerrainHelper
{
	public static float GetTerrainAltitude(Vector3 coord)
	{
		var terrain = Terrain.activeTerrain;
		return terrain.SampleHeight(coord) + terrain.GetPosition().y;
	}
}
